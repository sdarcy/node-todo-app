// const MongoClient = require('mongodb').MongoClient;

const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
    if(err){
        return console.log('Unable to connect to mongodb server')
    }
    console.log('Connected to MongoDb server');

    // deleteMany

    // db.collection('Users').deleteMany({name: 'Scot Darcy'}).then((result) => {
    //     console.log(result);
    // });

    // deleteOne

    // db.collection('Todos').deleteOne({text: 'Eat Lunch'}).then((result) => {
    //     console.log(result);
    // })

    // findOneAndDelete

    db.collection('Users').findOneAndDelete({_id: ObjectID('5accc8e62351d2c08faa52d6')}).then((result) => {
        console.log(result);
    })

    // db.close();
});
