// const MongoClient = require('mongodb').MongoClient;

const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
    if(err){
        return console.log('Unable to connect to mongodb server')
    }
    console.log('Connected to MongoDb server');

    // findOneAndUpdate

    // db.collection('Todos').findOneAndUpdate({
    //     _id: new ObjectID('5acdd282ee17f0b0860700cd')
    // }, {
    //     $set: {
    //         completed: true
    //     }
    // }, {
    //     returnOriginal: false
    // }).then((result) => {
    //     console.log(result);
    // })

    db.collection('Users').findOneAndUpdate({
        _id: new ObjectID("5accc8fa2351d2c08faa52e0")
        }, {
            $set : {
                name: 'Olivier'
            },
            $inc : {
                age: 1
            }
        }, {
            returnOriginal: false
        }
    ).then((result) => {
        console.log(result);
    })

    // db.close();
});
